// **** HIDE/SHOW LOGIN WINDOW **** //
var header_link_login = document.querySelector("#login-link");
var login_win_close_icon = document.querySelector("#close-login-window");
var login_win = document.querySelector("#login-window");
var login_win_gray_bkgr = document.querySelector("#gray-background");
var login_win_btn_reg = document.querySelector("#btn-login-register");
var login_win_form = document.querySelector("#form-login");
var login_win_btn_cancel = document.querySelector("#login-win-btn-cancel");
var login_social_icons = document.querySelectorAll("#login-window .login-social-icon");

header_link_login.addEventListener("click", function(event){
  // prevents page reload when clicking a link
  event.preventDefault();

  toggle_hide(login_win, login_win_gray_bkgr);
});
login_win_close_icon.addEventListener("click", function(){
  toggle_hide(login_win, login_win_gray_bkgr);
});
login_win_gray_bkgr.addEventListener("click", function(){
  toggle_hide(login_win, login_win_gray_bkgr);
});
login_win_btn_reg.addEventListener("click", function(event){
  // prevents page reload when clicking a link
  event.preventDefault();

  toggle_hide(login_win_btn_reg, login_win_form);
  toggle_social_icons(login_social_icons);
});
login_win_btn_cancel.addEventListener("click", function(event){
  // prevents page reload when clicking a link
  event.preventDefault();

  toggle_hide(login_win_btn_reg, login_win_form);
  toggle_social_icons(login_social_icons);
});

// accepts any number of elements as parameters, and toggles .hide-element css class on them
function toggle_hide(){
  for(i = 0; i < arguments.length; i++){
    arguments[i].classList.toggle("hide-element");
  };
};

// acceps a collection of elements as parameter, toggles .disabled css class on every element of collection
function toggle_social_icons (collection){
  // loops thru all elements of collection
  for(i = 0; i < collection.length; i++){
    // toggles .disabled css class
    collection[i].classList.toggle("disabled");
  };
};
// ^^^^ HIDE/SHOW LOGIN WINDOW ^^^^ //


// **** VALIDATE LOGIN CREDENTIALS **** //
var txtLogin = document.querySelector("#txtLogin");
var txtPass = document.querySelector("#txtPass");
var txtPassConfirm = document.querySelector("#txtPassConfirm");
var login_win_btn_save = document.querySelector("#login-win-btn-save");
var txtLoginError = document.querySelector("#txtLoginError");
var txtPassConfirmError = document.querySelector("#txtPassConfirmError");

// Disable Save btn on page load
login_win_btn_save.disabled = true;
// prevent firefox from "remembering" "ON" status of SAVE btn after page reload
login_win_btn_save.autocomplete = "off";

// addEventListeners to textboxes
txtLogin.addEventListener("input", validateCredentials);
txtPass.addEventListener("blur", validateCredentials);
txtPassConfirm.addEventListener("input", validateCredentials);

function validateCredentials (){
  // declare sentinel vars
  var loginValid = false;
  var passValid = false;

  // If login too short
  if (txtLogin.value.length < 2){
    // display red border
    txtLogin.style.border = "1.5px dashed red";
    // disable SAVE btn
    login_win_btn_save.disabled = true;
    // Display error message
    txtLoginError.innerHTML = "\t\t\tLogin too short";
  } else { // If login is okay
    // remore red border
    txtLogin.style.border = null;
    // remove error msg
    txtLoginError.innerHTML = "";
    // change sentinel var
    loginValid = true;
  };

  // If passwords match
  if (txtPass.value === txtPassConfirm.value){
    // remore red border
    txtPassConfirm.style.border = null;
    // remove error msg
    txtPassConfirmError.innerHTML = "";
    // change sentinel var
    passValid = true;
  } else { // If password don't match
    // dispay red border
    txtPassConfirm.style.border = "1.5px dashed red";
    // disable SAVE btn
    login_win_btn_save.disabled = true;
    // Display error message
    txtPassConfirmError.innerHTML = "\t\t\tPasswords do not match";
  };

  // If both sentinel vars true and password is not empty
  if (loginValid && passValid && txtPass.value !== ""){
    // Activate Save button
    login_win_btn_save.disabled = false;
  };
};

// ^^^^ VALIDATE LOGIN CREDENTIALS ^^^^ //


// **** DISPLAY LOGIN CREDENTIALS **** //

login_win_btn_save.addEventListener("click", function(){
  displayCredentials();

  // close login window, hide background, show REGISTER NOW btn, hide login form
  toggle_hide(login_win, login_win_gray_bkgr, login_win_btn_reg, login_win_form);
  // enable social icons
  toggle_social_icons(login_social_icons);
});

// Displays alert message with login credentials
function displayCredentials() {
  alert("Your login credentials:\n" + "Login: " + txtLogin.value + ", Password: " + txtPass.value);
};

// ^^^^ DISPLAY LOGIN CREDENTIALS ^^^^ //
